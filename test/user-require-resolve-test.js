/* eslint-env mocha */
'use strict'

const clearModule = require('clear-module')
const { cp, rm } = require('./test-helper.js')
const { expect } = require('chai')
const os = require('os')
const ospath = require('path')
const userRequire = require('..')

const CWD = process.cwd()
const FIXTURES_DIR = ospath.join(__dirname, 'fixtures')
const SAMPLE_NODE_MODULES = ospath.join(FIXTURES_DIR, 'sample_node_modules')
const NODE_MODULES = module.paths[1]
const WORK_DIR = ospath.join(__dirname, 'work')
const WORK_NODE_MODULES = ospath.join(WORK_DIR, 'node_modules')

describe('userRequire.resolve()', () => {
  describe('argument validation', () => {
    it('should throw error if request is undefined', () => {
      expect(() => userRequire.resolve()).to.throw(
        TypeError,
        'The "request" argument must be of type string. Received type undefined',
      )
    })

    it('should throw error if request is not a string', () => {
      expect(() => userRequire.resolve(true)).to.throw(
        TypeError,
        'The "request" argument must be of type string. Received type boolean',
      )
    })

    it('should throw error if request is empty', () => {
      expect(() => userRequire.resolve(''))
        .to.throw(Error, "Cannot find module ''")
        .with.property('code', 'MODULE_NOT_FOUND')
    })

    it('should show userRequire.resolve function call in stack', () => {
      expect(() => userRequire.resolve(''))
        .to.throw(Error, "Cannot find module ''")
        .with.property('stack')
        .that.includes(' at Function.userRequire.resolve ')
    })
  })

  describe('as path', () => {
    describe('absolute', () => {
      it('should resolve absolute path', () => {
        const input = ospath.join(FIXTURES_DIR, 'script.js')
        const expected = input
        const actual = userRequire.resolve(input)
        expect(actual).to.equal(expected)
      })

      it('should throw error if absolute path cannot be found', () => {
        const input = ospath.join(FIXTURES_DIR, 'does-not-exist.js')
        const expected = `Cannot find module '${input}'`
        expect(() => userRequire.resolve(input))
          .to.throw(Error, expected)
          .with.property('code', 'MODULE_NOT_FOUND')
      })
    })

    describe('relative', () => {
      it('should resolve relative path from value of base argument', () => {
        const input = 'script.js'
        const expected = ospath.join(FIXTURES_DIR, input)
        const actual = userRequire.resolve(input, { base: FIXTURES_DIR })
        expect(actual).to.equal(expected)
      })

      it('should throw error if relative path cannot be found', () => {
        const input = 'does-not-exist.js'
        const expected = `Cannot find module '${ospath.join(FIXTURES_DIR, input)}'`
        expect(() => userRequire.resolve(input, { base: FIXTURES_DIR })).to.throw(Error, expected)
      })

      it('should throw error if relative path cannot be found because base argument not specified', () => {
        const input = 'script.js'
        const expected = `Cannot find module '${ospath.join(process.cwd(), input)}'`
        expect(() => userRequire.resolve(input)).to.throw(Error, expected)
      })

      it('should throw error if relative path is missing a file extension', () => {
        const input = 'script'
        const expected = `Cannot find module '${input}'`
        expect(() => userRequire.resolve(input, { base: FIXTURES_DIR })).to.throw(Error, expected)
      })
    })

    describe('dot relative', () => {
      it('should resolve dot relative path from value of dot argument', () => {
        const input = './script.js'
        const expected = ospath.join(FIXTURES_DIR, input.substr(2))
        const actual = userRequire.resolve(input, { dot: FIXTURES_DIR })
        expect(actual).to.equal(expected)
      })

      it('should interpret dot relative path without file extension as path', () => {
        const input = './script'
        const expected = ospath.join(FIXTURES_DIR, input.substr(2) + '.js')
        const actual = userRequire.resolve(input, { dot: FIXTURES_DIR })
        expect(actual).to.equal(expected)
      })

      it('should interpret dot relative path without file extension in folder as path', () => {
        const input = './fixtures/script'
        const expected = ospath.join(ospath.dirname(FIXTURES_DIR), input.substr(2) + '.js')
        const actual = userRequire.resolve(input, { dot: ospath.dirname(FIXTURES_DIR) })
        expect(actual).to.equal(expected)
      })

      it('should throw error if dot relative path cannot be found', () => {
        const input = './does-not-exist.js'
        const expected = `Cannot find module '${ospath.join(FIXTURES_DIR, input.substr(2))}'`
        expect(() => userRequire.resolve(input, { dot: FIXTURES_DIR })).to.throw(Error, expected)
      })

      it('should throw error if dot relative path cannot be found because dot argument not specified', () => {
        const input = './script.js'
        const expected = `Cannot find module '${ospath.join(CWD, input.substr(2))}'`
        expect(() => userRequire.resolve(input)).to.throw(Error, expected)
      })
    })

    describe('cwd relative', () => {
      before(() => process.chdir(FIXTURES_DIR))

      after(() => process.chdir(CWD))

      it('should resolve cwd relative path from cwd', () => {
        const input = '~+/script.js'
        const expected = ospath.join(FIXTURES_DIR, input.substr(3))
        const actual = userRequire.resolve(input)
        expect(process.cwd()).to.equal(FIXTURES_DIR)
        expect(actual).to.equal(expected)
      })

      it('should throw error if cwd relative path cannot be found', () => {
        const input = '~+/does-not-exist.js'
        const expected = `Cannot find module '${ospath.join(FIXTURES_DIR, input.substr(3))}'`
        expect(() => userRequire.resolve(input)).to.throw(Error, expected)
      })
    })

    describe('~ relative', () => {
      const oldEnv = { ...process.env }

      before(() => Object.assign(process.env, { HOME: FIXTURES_DIR, USERPROFILE: FIXTURES_DIR }))

      after(() => {
        for (const name of Object.keys(process.env)) delete process.env[name]
        Object.assign(process.env, oldEnv)
      })

      it('should resolve ~ relative path from home directory', () => {
        const input = '~/script.js'
        const expected = ospath.join(FIXTURES_DIR, input.substr(2))
        const actual = userRequire.resolve(input)
        expect(os.homedir()).to.equal(FIXTURES_DIR)
        expect(actual).to.equal(expected)
      })

      it('should throw error if cwd relative path cannot be found', () => {
        const input = '~/does-not-exist.js'
        const expected = `Cannot find module '${ospath.join(FIXTURES_DIR, input.substr(2))}'`
        expect(() => userRequire.resolve(input)).to.throw(Error, expected)
      })
    })
  })

  describe('as module name', () => {
    describe('absolute', () => {
      it('should resolve absolute module name', () => {
        const input = ospath.join(NODE_MODULES, '@antora', 'expand-path-helper')
        const expected = require.resolve('@antora/expand-path-helper')
        const actual = userRequire.resolve(input)
        expect(actual).to.equal(expected)
      })

      it('should throw error if absolute module name cannot be found', () => {
        const input = ospath.join(NODE_MODULES, 'does-not-exist')
        const expected = `Cannot find module '${input}'`
        expect(() => userRequire.resolve(input))
          .to.throw(Error, expected)
          .with.property('code', 'MODULE_NOT_FOUND')
      })
    })

    describe('relative', () => {
      before(() => cp(SAMPLE_NODE_MODULES, WORK_NODE_MODULES))

      after(() => rm(WORK_DIR, { recursive: true, maxRetries: 10 }))

      it('should resolve relative module name from current project', () => {
        const input = '@antora/expand-path-helper'
        const expected = require.resolve(input)
        const actual = userRequire.resolve(input)
        expect(actual).to.equal(expected)
      })

      it('should resolve relative module name from calling script', () => {
        const expected = require.resolve('is-number-type', { paths: [WORK_DIR] })
        const resolvedModulePath = require.resolve('resolve-from-module', { paths: [WORK_DIR] })
        clearModule(resolvedModulePath)
        const actual = require(resolvedModulePath)()
        expect(actual).to.equal(expected)
      })

      it('should throw error if relative module name cannot be found', () => {
        const input = 'does-not-exist'
        const expected = `Cannot find module '${input}'`
        expect(() => userRequire.resolve(input)).to.throw(Error, expected)
      })

      it('should interpret path with file extension as module name if it ends with /', () => {
        const input = '@antora/expand-path-helper/lib/index.js/'
        const expected = require.resolve('@antora/expand-path-helper')
        const actual = userRequire.resolve(input)
        expect(actual).to.equal(expected)
      })
    })

    describe('relative to context', () => {
      before(() => cp(SAMPLE_NODE_MODULES, WORK_NODE_MODULES))

      after(() => rm(WORK_DIR, { recursive: true, maxRetries: 10 }))

      it('should resolve relative module name from resolved context', () => {
        const input = './work:is-number-type'
        const expected = require.resolve(input.substr(7), { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { dot: ospath.dirname(WORK_DIR) })
        expect(actual).to.equal(expected)
      })

      if (parseInt(process.versions.node.split('.')[0], 10) >= 16) {
        it('should not intercept the built-in node: prefix', () => {
          const input = 'node:fs'
          const actual = userRequire.resolve(input)
          expect(actual).to.equal(input)
        })
      }

      it('should throw error if relative module name cannot be found at resolved context', () => {
        const input = './work:does-not-exist'
        const expected = `Cannot find module '${input.substr(7)}' at path ${WORK_DIR}`
        expect(() => userRequire.resolve(input, { dot: ospath.dirname(WORK_DIR) }))
          .to.throw(Error, expected)
          .with.property('code', 'MODULE_NOT_FOUND')
      })

      it('should ignore specified paths when resolving relative module name from resolved context', () => {
        const input = './work:is-number-type'
        const expected = require.resolve(input.substr(7), { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { dot: ospath.dirname(WORK_DIR), paths: [FIXTURES_DIR] })
        expect(actual).to.equal(expected)
      })
    })

    describe('strict relative', () => {
      before(() => cp(SAMPLE_NODE_MODULES, WORK_NODE_MODULES))

      after(() => rm(WORK_DIR, { recursive: true, maxRetries: 10 }))

      it('should resolve strict relative module name from base argument', () => {
        const input = ':is-number-type'
        const expected = require.resolve('is-number-type', { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { base: WORK_DIR })
        expect(actual).to.equal(expected)
      })

      it('should resolve strict relative module name from base argument that uses path shorthand', () => {
        try {
          process.chdir(WORK_DIR)
          const input = ':is-number-type'
          const expected = require.resolve('is-number-type', { paths: [WORK_DIR] })
          const actual = userRequire.resolve(input, { base: '~+' })
          expect(actual).to.equal(expected)
        } finally {
          process.chdir(CWD)
        }
      })

      it('should resolve strict relative module name from cwd if base argument not specified', () => {
        try {
          process.chdir(WORK_DIR)
          const input = ':is-number-type'
          const expected = require.resolve(input.substr(1), { paths: [WORK_DIR] })
          const actual = userRequire.resolve(input)
          expect(actual).to.equal(expected)
        } finally {
          process.chdir(CWD)
        }
      })

      it('should interpret path with file extension and anchor as strict relative module name if it ends with /', () => {
        const input = ':is-number-type/index.js/'
        const expected = require.resolve('is-number-type', { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { base: WORK_DIR })
        expect(actual).to.equal(expected)
      })

      it('should throw error if strict relative module name cannot be found', () => {
        const input = ':does-not-exist'
        const expected = `Cannot find module '${input.substr(1)}' at path ${WORK_DIR}`
        expect(() => userRequire.resolve(input, { base: WORK_DIR })).to.throw(Error, expected)
      })

      it('should ignore specified paths when resolving strict relative module name', () => {
        const input = ':is-number-type'
        const expected = require.resolve(input.substr(1), { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { base: WORK_DIR, paths: [FIXTURES_DIR] })
        expect(actual).to.equal(expected)
      })
    })

    describe('calling script relative', () => {
      before(async () => {
        await cp(SAMPLE_NODE_MODULES, WORK_NODE_MODULES)
        process.chdir(WORK_DIR)
      })

      after(async () => {
        process.chdir(CWD)
        await rm(WORK_DIR, { recursive: true, maxRetries: 10 })
      })

      it('should resolve calling script relative module name', () => {
        const input = '^:@antora/expand-path-helper'
        const expected = require.resolve(input.substr(2))
        const actual = userRequire.resolve(input, { paths: [WORK_DIR] })
        expect(actual).to.equal(expected)
      })

      it('should throw error if calling script relative module name cannot be found', () => {
        const input = '^:does-not-exist'
        const expected = `Cannot find module '${input.substr(2)}'`
        expect(() => userRequire.resolve(input, { paths: [WORK_DIR] })).to.throw(Error, expected)
      })

      it('should ignore specified paths when resolving calling script relative module name', () => {
        const input = '^:@antora/expand-path-helper'
        const expected = require.resolve(input.substr(2))
        const actual = userRequire.resolve(input, { paths: [WORK_DIR] })
        expect(actual).to.equal(expected)
      })
    })

    describe('dot relative', () => {
      before(() => cp(SAMPLE_NODE_MODULES, WORK_NODE_MODULES))

      after(() => rm(WORK_DIR, { recursive: true, maxRetries: 10 }))

      it('should resolve dot relative module name from dot argument', () => {
        const input = '.:is-number-type'
        const expected = require.resolve(input.substr(2), { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { dot: WORK_DIR })
        expect(actual).to.equal(expected)
      })

      it('should resolve dot relative module name from dot argument that uses path shorthand', () => {
        try {
          process.chdir(WORK_DIR)
          const input = '.:is-number-type'
          const expected = require.resolve(input.substr(2), { paths: [WORK_DIR] })
          const actual = userRequire.resolve(input, { dot: '.' })
          expect(actual).to.equal(expected)
        } finally {
          process.chdir(CWD)
        }
      })

      it('should resolve dot relative module name from cwd if dot argument not specified', () => {
        try {
          process.chdir(WORK_DIR)
          const input = '.:is-number-type'
          const expected = require.resolve(input.substr(2), { paths: [WORK_DIR] })
          const actual = userRequire.resolve(input)
          expect(actual).to.equal(expected)
        } finally {
          process.chdir(CWD)
        }
      })

      it('should throw error if dot relative module name cannot be found', () => {
        const input = '.:does-not-exist'
        const expected = `Cannot find module '${input.substr(2)}' at path ${WORK_DIR}`
        expect(() => userRequire.resolve(input, { dot: WORK_DIR })).to.throw(Error, expected)
      })

      it('should ignore specified paths when resolving dot relative module name', () => {
        const input = '.:is-number-type'
        const expected = require.resolve(input.substr(2), { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { dot: WORK_DIR, paths: [FIXTURES_DIR] })
        expect(actual).to.equal(expected)
      })
    })

    describe('cwd relative', () => {
      before(async () => {
        await cp(SAMPLE_NODE_MODULES, WORK_NODE_MODULES)
        process.chdir(WORK_DIR)
      })

      after(async () => {
        process.chdir(CWD)
        await rm(WORK_DIR, { recursive: true, maxRetries: 10 })
      })

      it('should resolve cwd relative module name from cwd', () => {
        const input = '~+:is-number-type'
        const expected = require.resolve(input.substr(3), { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input)
        expect(process.cwd()).to.equal(WORK_DIR)
        expect(actual).to.equal(expected)
      })

      it('should throw error if dot relative module name cannot be found', () => {
        const input = '~+:does-not-exist'
        const expected = `Cannot find module '${input.substr(3)}' at path ${process.cwd()}`
        expect(() => userRequire.resolve(input)).to.throw(Error, expected)
      })

      it('should ignore specified paths when resolving cwd relative module name', () => {
        const input = '~+:is-number-type'
        const expected = require.resolve(input.substr(3), { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { paths: [FIXTURES_DIR] })
        expect(actual).to.equal(expected)
      })
    })

    describe('~ relative', () => {
      const oldEnv = { ...process.env }

      before(async () => {
        await cp(SAMPLE_NODE_MODULES, WORK_NODE_MODULES)
        process.chdir(WORK_DIR)
        Object.assign(process.env, { HOME: WORK_DIR, USERPROFILE: WORK_DIR })
      })

      after(async () => {
        process.chdir(CWD)
        await rm(WORK_DIR, { recursive: true, maxRetries: 10 })
        for (const name of Object.keys(process.env)) delete process.env[name]
        Object.assign(process.env, oldEnv)
      })

      it('should resolve ~ relative module name from home directory', () => {
        const input = '~:is-number-type'
        const expected = require.resolve(input.substr(2), { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input)
        expect(os.homedir()).to.equal(WORK_DIR)
        expect(actual).to.equal(expected)
      })

      it('should throw error if ~ relative module name cannot be found', () => {
        const input = '~:does-not-exist'
        const expected = `Cannot find module '${input.substr(2)}' at path ${os.homedir()}`
        expect(() => userRequire.resolve(input)).to.throw(Error, expected)
      })
    })

    describe('paths', () => {
      before(() => cp(SAMPLE_NODE_MODULES, WORK_NODE_MODULES))

      after(() => rm(WORK_DIR, { recursive: true, maxRetries: 10 }))

      it('should allow paths argument to control search path for bare module name', () => {
        const input = 'is-number-type'
        const expected = require.resolve('is-number-type', { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { paths: [WORK_DIR, module.paths[0]] })
        expect(actual).to.equal(expected)
      })

      it('should show all search paths in error if bare module name cannot be found', () => {
        const input = 'does-not-exist'
        const paths = [WORK_DIR, module.paths[0]]
        const expected = `Cannot find module '${input}' at path ${paths.join(' or ')}`
        expect(() => userRequire.resolve(input, { paths })).to.throw(Error, expected)
      })
    })

    describe('resolve function', () => {
      it('should use specified resolve function to resolve path', () => {
        const input = ospath.join(FIXTURES_DIR, 'script.js')
        let captured
        const resolve = (request) => (captured = request)
        const actual = userRequire.resolve(input, { resolve })
        expect(captured).to.equal(input)
        expect(actual).to.equal(captured)
      })

      it('should use specified resolve function to resolve module name', () => {
        const input = 'is-number-type'
        let captured
        const resolve = (request) => (captured = request)
        const actual = userRequire.resolve(input, { resolve })
        expect(captured).to.equal(input)
        expect(actual).to.equal(captured)
      })

      it('should use specified resolve function to resolve module name from context', () => {
        const input = '.:is-number-type'
        let captured
        const resolve = (...args) => (captured = args)[0]
        const actual = userRequire.resolve(input, { dot: FIXTURES_DIR, resolve })
        expect(captured).to.be.instanceOf(Array)
        expect(captured[0]).to.equal(input.substr(2))
        expect(actual).to.equal(captured[0])
        expect(captured[1].paths).to.have.lengthOf(1)
        // NOTE Node.js will automatically look in node_modules folder at specified path
        expect(captured[1].paths[0]).to.equal(FIXTURES_DIR)
      })
    })
  })
})
