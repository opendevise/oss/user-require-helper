/* eslint-env mocha */
'use strict'

const clearModule = require('clear-module')
const { cp, rm } = require('./test-helper.js')
const { expect } = require('chai')
const ospath = require('path')
const userRequire = require('..')

const FIXTURES_DIR = ospath.join(__dirname, 'fixtures')
const SAMPLE_NODE_MODULES = ospath.join(FIXTURES_DIR, 'sample_node_modules')
const WORK_DIR = ospath.join(__dirname, 'work')
const WORK_NODE_MODULES = ospath.join(WORK_DIR, 'node_modules')

describe('userRequire()', () => {
  before(() => cp(SAMPLE_NODE_MODULES, WORK_NODE_MODULES))

  after(() => rm(WORK_DIR, { recursive: true, maxRetries: 10 }))

  it('should show userRequire and userRequire.resolve function calls in stack', () => {
    expect(() => userRequire(''))
      .to.throw(Error, "Cannot find module ''")
      .with.property('stack')
      .that.includes(' at userRequire ')
      .and.that.includes(' at userRequire.resolve ')
  })

  it('should resolve and require path', () => {
    const input = './script.js'
    const expected = 'script called!'
    clearModule(ospath.join(FIXTURES_DIR, input.substr(2)))
    const actual = userRequire(input, { dot: FIXTURES_DIR })()
    expect(actual).to.equal(expected)
  })

  it('should resolve and require module name', () => {
    const input = '.:is-number-type'
    const expected = true
    clearModule(require.resolve(input.substr(2), { paths: [WORK_DIR] }))
    const actual = userRequire(input, { dot: WORK_DIR })(100)
    expect(actual).to.equal(expected)
  })

  it('should use specified resolve function to resolved id to require', () => {
    const input = '.:is-number-type'
    let captured
    const resolve = (...args) => require.resolve(...(captured = args))
    const expected = true
    clearModule(require.resolve(input.substr(2), { paths: [WORK_DIR] }))
    const actual = userRequire(input, { dot: WORK_DIR, resolve })(100)
    expect(actual).to.equal(expected)
    expect(captured[0]).to.equal(input.substr(2))
    expect(captured[1]).to.eql({ paths: [WORK_DIR] })
  })

  if (parseInt(process.versions.node.split('.')[0], 10) >= 16) {
    it('should resolve built-in module for module name with node: prefix', () => {
      try {
        const input = 'node:fs'
        const expected = require('fs')
        const fakeFs = {}
        require.cache.fs = { exports: fakeFs }
        const actual = userRequire(input)
        expect(actual).to.equal(expected)
      } finally {
        delete require.cache.fs
      }
    })
  }
})
