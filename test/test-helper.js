'use strict'

const { join } = require('path')
const { promises: fsp } = require('fs')

async function cp (from, to) {
  await fsp.mkdir(to, { recursive: true })
  const entries = await fsp.readdir(from, { withFileTypes: true })
  return Promise.all(
    entries.map((entry) => (entry.isDirectory() ? cp : fsp.copyFile)(join(from, entry.name), join(to, entry.name))),
  )
}

module.exports.cp = cp

if (!(module.exports.rm = fsp.rm)) {
  async function rmPolyfill (path, options = {}) {
    if (options.recursive) {
      try {
        const entries = await fsp.readdir(path, { withFileTypes: true })
        await Promise.all(
          entries.map((entry) => {
            if (entry.isDirectory()) return rmPolyfill(join(path, entry.name), options)
            return fsp.unlink(join(path, entry.name))
          }),
        )
        return fsp.rmdir(path)
      } catch (err) {
        if (err.code === 'ENOTDIR') return fsp.unlink(path)
        throw err
      }
    } else {
      try {
        return fsp.rmdir(path)
      } catch (err) {
        if (err.code === 'ENOTDIR') return fsp.unlink(path)
        throw err
      }
    }
  }

  module.exports.rm = rmPolyfill
}
