#!/bin/bash

VERSION=$1
if [ -z $VERSION ]; then
  echo You must specify a version.
  exit 1
fi

SCRIPT_PATH=$(readlink -f "$0")
SCRIPT_DIR=$(dirname "$SCRIPT_PATH")
PROJECT_DIR=$(dirname "$SCRIPT_DIR")
NAME=$(node -p "require('$PROJECT_DIR/package.json').name.replace(/^@/, '').replace('/', '-')")

npm version $VERSION
git push --tags origin main
node npm/prepublish.js
npm pack
node npm/postpublish.js
npm publish $NAME-$VERSION.tgz
rm -f $NAME-$VERSION.tgz
